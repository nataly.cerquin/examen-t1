﻿using Microsoft.AspNetCore.Mvc;
using N00019639.DB;
using N00019639.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00019639.Controllers
{

    public class PostDetalle
    {
        public Post Post { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }

    public class PostController : Controller
    {
        private AppBlogPostsContext context;

        public PostController(AppBlogPostsContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var posts = context.Posts;
            return View(posts);
        }

        [HttpGet]
        public IActionResult Detalle(int id)
        {
            var posts = context.Posts;

            var post = posts.FirstOrDefault(post => post.Id == id);
            List<Comentario> comentarios = context.Comentarios.Where(comentario => comentario.PostId == id).ToList();

            var detalle = new PostDetalle
            {
                Post = post,
                Comentarios = comentarios
            };

            return View(detalle);
        }

        [HttpGet]
        public IActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Crear(Post post)
        {
            post.FechaRedaccion = DateTime.Now;
            context.Posts.Add(post);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public RedirectToActionResult AddComentario(Comentario comentario)
        {
            context.Comentarios.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("Detalle", new { id = comentario.PostId });
        }
    }
}
