﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using N00019639.DB.Mapping;
using N00019639.Models;

namespace N00019639.DB
{
    public class AppBlogPostsContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }

        public AppBlogPostsContext(DbContextOptions<AppBlogPostsContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PostMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
        }
    }
}
