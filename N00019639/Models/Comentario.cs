﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace N00019639.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Detalle { get; set; }
    }
}
